fn main() {
    tonic_build::configure()
        .protoc_arg("--experimental_allow_proto3_optional")
        .compile(&["v0/service.proto"], &["proto/check-if-email-exists"])
        .unwrap();
}
