# Check If Email Exists GRPC

## Gotchas

### Firewalls

As per the [docs](https://github.com/reacherhq/check-if-email-exists#what-does-is_reachable-unknown-mean), `check-if-email-exists` needs to use port 25.

This works fine in cloud instances, but the firewall in Robot makes the query fail because it can't use the port. For now, this chart uses affinity rules to make sure that pods are scheduled in cloud instances. See [task](https://projects.zoho.eu/portal/disruptor#taskdetail/123674000000026005/123674000000063025/123674000000160001)

### Linkerd

On top of that, Linkerd can't perform protocol detection on smtp ports. They add [port SMTP 587](https://linkerd.io/2.10/tasks/upgrading-2.10-ports-and-protocols/) as one of the defaulted opaque ports.

This charts also adds port 25, as that's what `check-if-email-exists` uses