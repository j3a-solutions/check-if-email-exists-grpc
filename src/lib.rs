#[macro_use]
extern crate tracing;

pub mod proto {
    pub mod v0 {
        tonic::include_proto!("check_if_email_exists.v0");
    }
}

use std::convert::TryFrom;

use check_if_email_exists::{CheckEmailOutput, Reachable as ReachableHttp};
use proto::v0::{
    check_if_email_exists_response::{Misc, Mx, Smtp},
    CheckIfEmailExistsRequest, CheckIfEmailExistsResponse, MiscDetails, MxDetails,
    Reachable as ReachableProto, SmtpDetails, SyntaxDetails,
};
use tonic::Status;

// Helper function to bring in the default values of check_if_email_exists
// Unfortunately, prost implements Default already. See https://github.com/tokio-rs/prost/issues/334
impl CheckIfEmailExistsRequest {
    pub fn with_default_values() -> Self {
        let d = check_if_email_exists::CheckEmailInput::default();

        Self {
            to_emails: d.to_emails,
            from_email: d.from_email,
            hello_name: d.hello_name,
            proxy: None,
            smtp_port: Some(d.smtp_port as _),
            smtp_timeout: None,
            yahoo_use_api: d.yahoo_use_api,
        }
    }
}

impl From<&ReachableHttp> for ReachableProto {
    fn from(value: &ReachableHttp) -> Self {
        match value {
            ReachableHttp::Safe => ReachableProto::Safe,
            ReachableHttp::Risky => ReachableProto::Risky,
            ReachableHttp::Invalid => ReachableProto::Invalid,
            ReachableHttp::Unknown => ReachableProto::Unknown,
        }
    }
}

impl TryFrom<CheckEmailOutput> for CheckIfEmailExistsResponse {
    type Error = Status;

    fn try_from(value: CheckEmailOutput) -> Result<Self, Self::Error> {
        debug!(?value, "Serialising validation output");

        let syntax = SyntaxDetails {
            email_address: value.syntax.address.map(|x| x.to_string()),
            domain: value.syntax.domain,
            is_valid_syntax: value.syntax.is_valid_syntax,
            username: value.syntax.username,
        };

        let misc = value
            .misc
            .as_ref()
            .map(|m| {
                Misc::MiscDetails(MiscDetails {
                    is_disposable: m.is_disposable,
                    is_role_account: m.is_role_account,
                })
            })
            .unwrap_or_else(|err| {
                // NOTE: Unable to determine if this err is internal or due to invalid email
                Misc::MiscError(serde_json::to_string(&err).unwrap_or_else(|error| {
                    error!(?error, "Unable to serialize MiscError");
                    "Unable to serialize MiscError".to_string()
                }))
            });

        let mx = value
            .mx
            .as_ref()
            .map_err(|err| {
                // NOTE: Unable to determine if this err is internal or due to invalid email
                Mx::MxError(serde_json::to_string(&err).unwrap_or_else(|error| {
                    error!(?error, "Unable to serialize MxError");
                    "Unable to serialize MxError".to_string()
                }))
            })
            .and_then(|mx| {
                mx.lookup
                    .as_ref()
                    .map_err(|err| Mx::MxError(err.to_string()))
            })
            .map(|lookup| {
                // https://github.com/reacherhq/check-if-email-exists/blob/ae270d588a0707bbe0af4b22872f10772e8b3ecf/core/src/mx.rs#L44
                let records = lookup
                    .iter()
                    .map(|host| host.exchange().to_string())
                    .collect::<Vec<_>>();

                Mx::MxDetails(MxDetails {
                    accepts_mail: !records.is_empty(),
                    records,
                })
            })
            .unwrap_or_else(|err| err);

        let smtp = value
            .smtp
            .as_ref()
            .map(|smtp| {
                Smtp::SmtpDetails(SmtpDetails {
                    can_connect_smtp: smtp.can_connect_smtp,
                    has_full_inbox: smtp.has_full_inbox,
                    is_catch_all: smtp.is_catch_all,
                    is_deliverable: smtp.is_deliverable,
                    is_disabled: smtp.is_disabled,
                })
            })
            .unwrap_or_else(|err| {
                // NOTE: Unable to determine if this err is internal or due to invalid email
                Smtp::SmtpError(serde_json::to_string(&err).unwrap_or_else(|error| {
                    error!(?error, "Unable to serialize SmtpError");
                    "Unable to serialize SmtpError".to_string()
                }))
            });

        Ok(Self {
            input: value.input,
            is_reachable: ReachableProto::from(&value.is_reachable) as i32,
            syntax: Some(syntax),
            misc: Some(misc),
            mx: Some(mx),
            smtp: Some(smtp),
        })
    }
}
