#[macro_use]
extern crate tracing;

#[macro_use]
extern crate async_recursion;

use check_if_email_exists::{check_email, CheckEmailInput, CheckEmailInputProxy};
use check_if_email_exists_grpc::proto::v0::{
    service_server::{Service, ServiceServer},
    CheckIfEmailExistsRequest, CheckIfEmailExistsResponse,
};
use prost::{bytes::Bytes, Message};
use redis::{
    aio::ConnectionManager, AsyncCommands, ConnectionAddr, ConnectionInfo, RedisConnectionInfo,
};
use std::{
    convert::TryFrom,
    net::SocketAddr,
    time::{Duration, Instant},
};
use tokio::{sync::mpsc, time::sleep};
use tokio_stream::wrappers::ReceiverStream;
use tonic::{transport::Server, Request, Response, Status};
use tracing::Instrument;
use tracing_subscriber::prelude::*;
use tracing_subscriber::EnvFilter;

fn take<T>(mut vec: Vec<T>, index: usize) -> Option<T> {
    if vec.get(index).is_none() {
        None
    } else {
        Some(vec.swap_remove(index))
    }
}

#[async_recursion]
async fn redis_connection(client: redis::Client) -> ConnectionManager {
    match ConnectionManager::new(client.clone()).await {
        Ok(c) => {
            info!("Connected to Redis");
            c
        }
        Err(error) => {
            error!(
                ?error,
                "Unable to create Redis connection manager. Retrying in 5 seconds"
            );
            sleep(Duration::from_secs(5)).await;
            redis_connection(client.clone()).await
        }
    }
}

type CheckIfEmailExistsStream = ReceiverStream<Result<CheckIfEmailExistsResponse, Status>>;

struct GrpcServer {
    redis_connection: ConnectionManager,
    key_exp_time_seconds: usize,
}

impl GrpcServer {
    pub async fn new() -> anyhow::Result<Self> {
        let redis_host: String = std::env::var("REDIS_HOST")
            .unwrap_or_else(|_| "127.0.0.1".into())
            .parse()?;

        let redis_port: u16 = std::env::var("REDIS_PORT")
            .unwrap_or_else(|_| "6379".into())
            .parse()?;

        let db: i64 = std::env::var("REDIS_DB")
            .unwrap_or_else(|_| "0".into())
            .parse()?;

        let key_exp_time_seconds: usize = std::env::var("CACHE_EXP_SECONDS")
            .unwrap_or_else(|_| "86400".into())
            .parse()?;

        info!(
            host = %redis_host,
            port = redis_port,
            db,
            "Connecting to Redis"
        );

        let redis_client = redis::Client::open(ConnectionInfo {
            addr: ConnectionAddr::Tcp(redis_host, redis_port),
            redis: RedisConnectionInfo {
                db,
                username: std::env::var("REDIS_USERNAME").ok(),
                password: std::env::var("REDIS_PASSWORD").ok(),
            },
        })?;

        let redis_connection = redis_connection(redis_client.clone()).await;

        Ok(GrpcServer {
            redis_connection,
            key_exp_time_seconds,
        })
    }

    async fn handler(
        &self,
        request: Request<CheckIfEmailExistsRequest>,
    ) -> Result<Response<CheckIfEmailExistsStream>, Status> {
        info!("Request received");
        let request_time = Instant::now();
        let input = request.into_inner();
        let (tx, rx) = mpsc::channel(input.to_emails.len());

        let key_exp_time_seconds = self.key_exp_time_seconds;
        let mut redis = self.redis_connection.clone();

        tokio::spawn(async move {
            for email in input.to_emails {
                let time_before_cache = Instant::now();
                let cached_value = redis.get::<&str, Bytes>(&email).await.map_or_else(
                    |error| {
                        info!(%error, latency = ?time_before_cache.elapsed(), "No cache hit");
                        None
                    },
                    |bytes| {
                        info!(latency = ?time_before_cache.elapsed(), "Cache hit");
                        CheckIfEmailExistsResponse::decode(bytes)
                            .map_err(|error| {
                                error!(%error, "Unable to parse cached value");
                            })
                            .ok()
                    },
                );

                let result = match cached_value {
                    Some(value) => Ok(value),
                    None => {
                        let input_http = CheckEmailInput {
                            to_emails: vec![email.clone()],
                            from_email: input.from_email.clone(),
                            hello_name: input.hello_name.clone(),
                            proxy: input.proxy.as_ref().map(|p| CheckEmailInputProxy {
                                host: p.host.clone(),
                                port: p.port as u16,
                            }),
                            smtp_port: input
                                .smtp_port
                                .unwrap_or(CheckEmailInput::default().smtp_port as _)
                                as _,
                            smtp_timeout: input.smtp_timeout.map(Duration::from_secs),
                            yahoo_use_api: input.yahoo_use_api,
                        };

                        debug!(input=?input_http, "Validating input");

                        let time_before_check = Instant::now();
                        let results = check_email(&input_http).await;

                        let result = take(results, 0)
                            .ok_or_else(|| {
                                Status::internal(
                                    "check_if_email_exists should have returned one result",
                                )
                            })
                            .and_then(CheckIfEmailExistsResponse::try_from);

                        info!(%email, elapsed = ?time_before_check.elapsed(), "Validated email");

                        if let Ok(value) = &result {
                            redis
                                .set_ex::<&str, Vec<u8>, ()>(
                                    &email,
                                    value.encode_to_vec(),
                                    key_exp_time_seconds,
                                )
                                .await
                                .map_or_else(
                                    |error| {
                                        error!(
                                            ?error,
                                            "Unable to write validation result to Redis"
                                        );
                                    },
                                    |_| {
                                        debug!("Stored email validation result in cache");
                                    },
                                );
                        }

                        result
                    }
                };

                if tx.send(result).await.is_err() {
                    error!("receiver dropped");
                    return;
                }

                info!(latency = ?request_time.elapsed(), "Request ended");
            }
        });

        Ok(Response::new(ReceiverStream::new(rx)))
    }
}

#[tonic::async_trait]
impl Service for GrpcServer {
    type CheckIfEmailExistsStream = CheckIfEmailExistsStream;

    async fn check_if_email_exists(
        &self,
        request: Request<CheckIfEmailExistsRequest>,
    ) -> Result<Response<Self::CheckIfEmailExistsStream>, Status> {
        self.handler(request)
            .instrument(info_span!("check_if_email_exists"))
            .await
    }
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let filter_layer = EnvFilter::try_from_default_env()
        .or_else(|_| EnvFilter::try_new("check_if_email_exists_grpc=info"))
        .unwrap();

    tracing_subscriber::registry()
        .with(filter_layer)
        .with(tracing_subscriber::fmt::layer())
        .try_init()?;

    debug!("Starting service");

    let service = ServiceServer::new(GrpcServer::new().await?);
    let address: SocketAddr = std::env::var("BIND_ADDRESS")
        .unwrap_or_else(|_| "0.0.0.0:50050".into())
        .parse()?;

    let (mut health_reporter, health_service) = tonic_health::server::health_reporter();
    health_reporter
        .set_serving::<ServiceServer<GrpcServer>>()
        .await;

    info!(%address, "Grpc server started");

    Server::builder()
        .add_service(health_service)
        .add_service(service)
        .serve(address)
        .await?;

    Ok(())
}
