# Check If Email Exists GRPC

This crate is a wrapper around the HTTP version of [check-if-email-exists](https://github.com/reacherhq/check-if-email-exists). It exposes a Grpc API instead of Rest.

If you are looking for a SaaS solution for your email validation, then please check [Reacher](https://reacher.email/) by the original creator of `check-if-email-exists`.

Why would you use this version?

- To enjoy the benefits of Grpc performance vs Http in a microservice architecture
- To increase performance even more by using a Redis cache. This would be useful if the same email is validated more than once in a short period of time
- You need observability and audit trails for email validation in your pipelines. This crate uses tracing for that

## How to run

### Docker container

There is a docker image [here](https://hub.docker.com/r/j3asolutions/check-if-email-exists-grpc). It supports the same environment variables than the executable.

If you're using Docker as container runtime, then you may want to use the `--init` option in order to pass signals to the process.

### Kubernetes

See the helm chart in this repo

### Executable

For this, you'd need to build the application using `cargo build`

## How to connect clients

### Using an auto-generated grpc client (no Rust needed!)

The `proto` files are published in the [Buf Schema Registry](https://buf.build/j3a-solutions/check-if-email-exists). You can then use Buf to generate the code in the language of your choice. Just follow the [docs](https://docs.buf.build/generate/usage/)

### In Rust

This crate exports a grpc client in the module `proto::v0::service_client`

### How to configure

There are several environment variables:


| BIND_ADDRESS      | Description                                                     | Default Value                   |
|-------------------|-----------------------------------------------------------------|---------------------------------|
| REDIS_HOST        | Redis IP                                                        | 127.0.0.1                       |
| REDIS_PORT        | Redis port                                                      | 6379                            |
| REDIS_DB          | Redis database to connect to                                    | 0                               |
| REDIS_USERNAME    | Redis username                                                  | ""                              |
| REDIS_PASSWORD    | Redis password                                                  | ""                              |
| CACHE_EXP_SECONDS | How long each validation output is stored in Redis (in seconds) | 86400                           |
| RUST_LOG          | Logging level                                                   | check_if_email_exists_grpc=info |

## License

This project is published under the AGPL-3.0, or Affero General Public License, version 3, as per the license requirements of [check-if-email-exists](https://github.com/reacherhq/check-if-email-exists)
